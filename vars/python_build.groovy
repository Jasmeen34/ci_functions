def call (dockerRepoName, imageName, portNum) {
    pipeline {
      environment {
      SYSDIG_API_TOKEN_CRED = credentials('sysdig-secure-api-token')
      api_endpoint = 'https://eu1.app.sysdig.com'
    myimage = ''
  }
    agent any
    stages {
        stage('Build') {
            steps {
                sh 'pip install -r requirements.txt'
                
            }
        }
    	stage('Lint'){
            	steps{
                	sh 'pylint-fail-under --fail_under 5.0 *.py'
            	}

        }

        stage('Package') {
            steps {
                withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]) {
                sh "docker login -u 'jasmeen34' -p '$TOKEN' docker.io"
                sh "docker build -t ${dockerRepoName}:latest --tag jasmeen34/${dockerRepoName}:${imageName} ."
                sh "docker push jasmeen34/${dockerRepoName}:${imageName}"
                }
            }
        }

        stage('Scanning images') {
            steps {
                withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]) {
                    sh "docker login -u 'jasmeen34' -p '$TOKEN' docker.io"
                    sh "docker scan --accept-license --severity=high jasmeen34/${dockerRepoName}:${imageName}"
                }
            }
        }
    stage ('deploy') {
            steps{
                sshagent(credentials : ['sshkey']) {
                    sh 'ssh -o StrictHostKeyChecking=no azureuser@acit3855-kafka-lab6.eastus.cloudapp.azure.com docker compose -f /home/azureuser/assign-3/deployment/docker-compose.yml down'
                    sh 'ssh -o StrictHostKeyChecking=no azureuser@acit3855-kafka-lab6.eastus.cloudapp.azure.com docker images'
                    sh "ssh -o StrictHostKeyChecking=no azureuser@acit3855-kafka-lab6.eastus.cloudapp.azure.com docker rmi jasmeen34/${dockerRepoName}:${imageName}"
                    sh "ssh -o StrictHostKeyChecking=no azureuser@acit3855-kafka-lab6.eastus.cloudapp.azure.com docker pull jasmeen34/${dockerRepoName}:${imageName}"
                    sh 'ssh -o StrictHostKeyChecking=no azureuser@acit3855-kafka-lab6.eastus.cloudapp.azure.com docker compose -f /home/azureuser/assign-3/deployment/docker-compose.yml up -d'
                    sh 'ssh -o StrictHostKeyChecking=no azureuser@acit3855-kafka-lab6.eastus.cloudapp.azure.com docker ps'

                }
            }
        }

   }	
}


}
